from django.contrib import admin
from .models import Pool


class PoolAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'coin')


# Register your models here.
admin.site.register(Pool, PoolAdmin)

