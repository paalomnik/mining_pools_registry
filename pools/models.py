from django.db import models
from coins.models import Coin


# Create your models here.
class Pool(models.Model):
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=500)
    coin = models.ForeignKey(Coin, null=True)
    stat_url = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return self.name
