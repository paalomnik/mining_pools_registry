from django.core.management.base import BaseCommand, CommandError
from pools.models import Pool
import requests


class Command(BaseCommand):
    help = 'Sync for pools statistics'

    def handle(self, *args, **options):
        pool_set = Pool.objects.all()
        pool_iterator = pool_set.iterator()
        try:
            f_pool = next(pool_iterator)
        except StopIteration:
            self.stdout.write(self.style.WARNING('No pools found'))
        else:
            from itertools import chain
            for pool in chain([f_pool], pool_iterator):
                self.stdout.write(self.style.SUCCESS('Processing {pool}'.format(pool=pool.name)))
                # if 'stat_url' not in pool:

                response = requests.get(pool.stat_url)
