from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from .models import Pool


# Create your views here.
def homepage(request):
    page = request.GET.get('page', 1)
    pool_list = Pool.objects.all()
    paginator = Paginator(pool_list, settings.ROWS_PER_PAGE or 30)
    try:
        pools = paginator.page(page)
    except PageNotAnInteger:
        pools = paginator.page(1)
    except EmptyPage:
        pools = paginator.page(paginator.num_pages)

    return render(request, 'pools/home.html', {'pools': pools})
