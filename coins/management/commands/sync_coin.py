import json, re
from urllib.request import urlopen, Request
from urllib.error import HTTPError
from bs4 import BeautifulSoup
from bs4.element import NavigableString
from django.core.management.base import BaseCommand
from django.core.files.base import ContentFile
from coins.models import Coin, Algorithm
from pprint import pprint


class Command(BaseCommand):
    help = 'Sync coins from coinmarketcap and whattomine'
    existing_coins = []
    ua = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    page_number = 1

    def get_soup(self, url):
        req = Request(
            url,
            data=None,
            headers={
                'User-Agent': self.ua
            }
        )
        return BeautifulSoup(urlopen(req), 'html.parser')

    def get_coin_website(self, link_object):
        website = ''
        if link_object:
            soup = self.get_soup('https://coinmarketcap.com{uri}'.format(uri=link_object.get('href')))
            website_link = soup.find('span', {'class': 'glyphicon-link'})
            try:
                for sibling in website_link.next_siblings:
                    if isinstance(sibling, NavigableString):
                        continue

                    website = sibling.get('href')
                    break
            except AttributeError:
                pass

        return website

    def handle(self, *args, **options):
        with urlopen('https://api.coinmarketcap.com/v2/listings/') as response:
            coins = json.loads(response.read().decode())
            if 'data' not in coins:
                self.stdout.write(self.style.ERROR('No coins data from coinmarketcap'))
                return

            for c in coins['data']:
                self.stdout.write(self.style.SUCCESS('{coin} processing.'.format(coin=c['name'])))
                Coin.objects.get_or_create(
                    name=c['name'],
                    symbol=c['symbol'],
                    slug=c['website_slug'],
                    coinmarketcap_id=c['id']
                )

        req = Request(
            'http://whattomine.com/coins.json',
            data=None,
            headers={
                'User-Agent': self.ua
            }
        )
        with urlopen(req) as response:
            coins = json.loads(response.read().decode())
            if 'coins' not in coins:
                self.stdout.write(self.style.ERROR('No coins data from whattomine GPU'))

            for c in coins['coins']:
                coin_data = coins['coins'][c]
                if 'algorithm' in coin_data:
                    algo, created = Algorithm.objects.get_or_create(name=coin_data['algorithm'])
                    coin = Coin.objects.filter(name=re.sub("([a-z])([A-Z])", "\g<1> \g<2>", c)).first()
                    if coin:
                        coin.algorithm = algo
                        coin.save()
        req = Request(
            'http://whattomine.com/asic.json',
            data=None,
            headers={
                'User-Agent': self.ua
            }
        )
        with urlopen(req) as response:
            coins = json.loads(response.read().decode())

            if 'coins' not in coins:
                self.stdout.write(self.style.ERROR('No coins data from whattomine ASIC'))

            for c in coins['coins']:
                if 'nisehash' in c.lower():
                    continue
                coin_data = coins['coins'][c]
                if 'algorithm' in coin_data:
                    algo, created = Algorithm.objects.get_or_create(name=coin_data['algorithm'])
                    coin = Coin.objects.filter(name=re.sub("([a-z])([A-Z])", "\g<1> \g<2>", c)).first()
                    if coin:
                        coin.algorithm = algo
                        coin.save()

        while True:
            try:
                soup = self.get_soup('https://coinmarketcap.com/{page}'.format(page=self.page_number))
            except HTTPError:
                self.stdout.write(self.style.WARNING('No more pages.'))
                break

            tbody = soup.find('tbody')
            for row in tbody.find_all('tr'):
                symbol = row.find('span', {'class': 'currency-symbol'})
                if symbol:
                    symbol = symbol.text
                    coin = Coin.objects.filter(symbol=symbol)
                    if not coin.exists():
                        self.stdout.write(self.style.SUCCESS('{symbol} does not exists.'.format(symbol=symbol)))
                        continue
                    website = self.get_coin_website(row.find('a', {'class': 'currency-name-container'}))
                    coin = coin.first()
                    logo_tag = row.find('img', {'class': 'logo-sprite'})
                    if not logo_tag:
                        self.stdout.write(self.style.SUCCESS('{symbol} logo does not exists.'.format(symbol=symbol)))
                        continue
                    logo_url = logo_tag.get('data-src') or logo_tag.get('src')

                    with urlopen(logo_url) as r:
                        if r.code == 200:
                            img_name = coin.name.lower().replace(' ', '_') + '.jpg'
                            content = ContentFile(r.read())
                            coin.logo_small.save(img_name, content, save=True)

                    coin.website = website
                    coin.mineable = '*' not in row.find('td', {'class': 'circulating-supply'}).text
                    coin.save()
                else:
                    self.stdout.write(self.style.ERROR('{symbol} does not exists in row.'))

            self.page_number += 1
            self.stdout.write(self.style.SUCCESS('Switching to {page_number}'.format(page_number=self.page_number)))

