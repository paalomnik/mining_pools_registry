from django.contrib import admin
from .models import Coin, FiatCurrency, Algorithm


class CoinAdmin(admin.ModelAdmin):
    list_display = ('name', 'algorithm', 'website', 'mineable')


# Register your models here.
admin.site.register(Coin, CoinAdmin)
admin.site.register(FiatCurrency)
admin.site.register(Algorithm)
