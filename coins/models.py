from django.db import models


# Create your models here.
class Algorithm(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Coin(models.Model):
    name = models.CharField(max_length=300)
    symbol = models.CharField(max_length=10, db_index=True)
    slug = models.CharField(max_length=300, unique=True)
    mineable = models.BooleanField(default=True)
    logo_small = models.ImageField(upload_to='media/', default=None, blank=True)
    coinmarketcap_id = models.SmallIntegerField(db_index=True, unique=True)
    algorithm = models.ForeignKey(Algorithm, blank=True, null=True)
    website = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.name


class FiatCurrency(models.Model):
    name = models.CharField(max_length=3)

    def __str__(self):
        return self.name



