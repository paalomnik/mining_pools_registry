from django.conf.urls import url
from . import views

app_name = 'coins'

urlpatterns = [
    url(r'^$', views.coin_list, name='coin_list'),
]
