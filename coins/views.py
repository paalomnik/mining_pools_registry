from django.shortcuts import render
from .models import Coin
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from pprint import pprint


# Create your views here.
def coin_list(request):
    page = request.GET.get('page', 1)
    coin_list = Coin.objects.filter(algorithm__isnull=False)
    paginator = Paginator(coin_list, settings.ROWS_PER_PAGE or 30)
    try:
        coins = paginator.page(page)
    except PageNotAnInteger:
        coins = paginator.page(1)
    except EmptyPage:
        coins = paginator.page(paginator.num_pages)

    return render(request, 'coins/list.html', {'coins': coins})
